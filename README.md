# Lab 2 -- Linter and SonarQube as a part of quality gates

## SonarCloud Checks

### Main screen
![](./screenshots/main.png)

### Summary
![](./screenshots/summary.png)

### An example
![](./screenshots/example.png)
